#!/usr/bin/env python


from bal_model import Asset, Pool
import csv
import datetime as dt
import matplotlib.dates as mdate
import matplotlib.pyplot as plt
import math
import numpy as np


# constants
bal_reward = 0.2  # annual percentage of liquidity
csv_filename = ''
gas_fees = 5.0  # USD
init_liquidity = 10000.0  # USD
parameters = 2
plot = True
plot_legend_font_size = 6
plot_legend_line_width = 1
plot_line_width = 1
plot_margin_bottom = 0.06
plot_margin_horz = 0.1
plot_margin_left = 0.12
plot_margin_right = 0.02
plot_margin_top = 0.06
plot_margin_vert = 0.05
write = True

# parameters
if parameters == 0:
    # ETH/USD arb-only w/ various fees
    data_per_day = 24
    plot_bal_rewards = False
    plot_eth = True
    plot_eth_balance = False
    plot_hodl = True
    plot_impermanent_loss = True
    use_truncated_data = True
    plot_title = 'ETH/USD Balancer Pool Fee Comparison'
    if use_truncated_data:
        plot_filename = 'eth_usd_fee_comp_trunc.png'
    else:
        plot_filename = 'eth_usd_fee_comp.png'
    pool_assets = [ 'ETH', 'USD' ]
    pool_weights = [ 0.5, 0.5 ]
    min_profits = [ 5.0 ]
    # trade_fees = [ 0.0, 0.003, 0.01, 0.05, 0.1 ]
    trade_fees = [ 0.003, 0.05, 0.1 ]
    daily_volumes = [ 0.0 ]

elif parameters == 1:
    # Sets arb-only w/ high fee
    data_per_day = 24
    plot_bal_rewards = False
    plot_eth = False
    plot_eth_balance = False
    plot_hodl = True
    plot_impermanent_loss = True
    use_truncated_data = False
    plot_title = 'Set of Sets - various fees, arbitrage only'
    plot_filename = 'set_arb.png'
    pool_assets = [ 'ETHBTCRSI', 'ETHUSDRSI' ]
    pool_weights = [ 0.5, 0.5 ]
    min_profits = [ 5.0 ]
    trade_fees = [ 0.002, 0.005, 0.01, 0.1 ]
    daily_volumes = [ 0.0 ]

else:
    # Sets/ETH fixed low fee w/ various volumes
    data_per_day = 24
    plot_bal_rewards = False
    plot_eth = False
    plot_eth_balance = False
    plot_hodl = True
    plot_impermanent_loss = True
    use_truncated_data = False
    plot_title = 'Set of Sets - 0.25% fee, various daily volumes as % of liquidity'
    plot_filename = 'set_volume.png'
    pool_assets = [ 'ETHBTCRSI', 'ETHUSDRSI', 'ETH' ]
    pool_weights = [ 0.4, 0.4, 0.2 ]
    min_profits = [ 5.0 ]
    trade_fees = [ 0.0025 ]
    daily_volumes = [ 0.0, 0.05, 0.1, 0.15 ]


# helper functions
def reset_plot_color():
    return 1 if plot_eth else 0

def cycle_plot_color(color):
    color += 1
    return color if color < 10 else reset_plot_color()


# all_assets = [ 'ETH', 'BTC', 'USD' ]
all_assets = [ 'ETH', 'USD' ]
for asset in pool_assets:
    if asset not in all_assets:
        all_assets.append(asset)

# initialize HODL positions
num_assets = len(pool_assets)
hodl_assets = []
for asset in all_assets:
    hodl_assets.append(Asset(asset))

# initialize loop variables
bal_rewards = {}
eth_balances = {}
pool_liquidities = {}
trade_times = {}

# compute bal percentage earned per period
bal_pct = (1.0 + bal_reward) ** (1.0 / (365.0 * data_per_day)) - 1


# loop
for min_profit in min_profits:

    bal_rewards[min_profit] = {}
    eth_balances[min_profit] = {}
    pool_liquidities[min_profit] = {}
    trade_times[min_profit] = {}

    for trade_fee in trade_fees:

        bal_rewards[min_profit][trade_fee] = {}
        eth_balances[min_profit][trade_fee] = {}
        pool_liquidities[min_profit][trade_fee] = {}
        trade_times[min_profit][trade_fee] = {}

        for daily_volume_pct in daily_volumes:

            # correct assumed hourly volume to daily
            daily_volume_corr = (1.0 + daily_volume_pct) ** (24 / data_per_day) - 1

            bal_rewards[min_profit][trade_fee][daily_volume_pct] = []
            eth_balances[min_profit][trade_fee][daily_volume_pct] = []
            pool_liquidities[min_profit][trade_fee][daily_volume_pct] = []
            trade_times[min_profit][trade_fee][daily_volume_pct] = []
            # compute fee factor
            fee_factor = math.exp(-((100.0 * trade_fee / 2.0) ** 2))

            # compute ratio factor
            ratioFactorSum = 0
            pairWeightSum = 0
            n = len(pool_weights)
            for i in range(0, n):
                for j in range(i+1, n):
                    pairWeight = pool_weights[i] * pool_weights[j]
                    normalizedWeight1 = pool_weights[i] / (pool_weights[i] + pool_weights[j])
                    normalizedWeight2 = pool_weights[j] / (pool_weights[i] + pool_weights[j])
                    ratioFactorSum += (4 * normalizedWeight1 * normalizedWeight2) * pairWeight
                    pairWeightSum += pairWeight
            ratio_factor = ratioFactorSum / pairWeightSum

            # TODO: comment
            # do other stuff

            hodl_liquidities = []
            prices = {}
            times = []
            trades = 0

            for asset in all_assets:
                prices[asset] = []

            # initialize the unchanging reference pool and rebalancing pool
            init_pool = Pool(daily_volume_corr, min_profit + gas_fees, trade_fee)
            pool = Pool(daily_volume_corr, min_profit + gas_fees, trade_fee)
            for i in range(0, num_assets):
                init_pool.add_asset(pool_assets[i], pool_weights[i])
                pool.add_asset(pool_assets[i], pool_weights[i])
            init_pool.update_weights()
            pool.update_weights()

            first_pass = True

            # open CSV for reading (hourly price data)
            if not csv_filename:
                if use_truncated_data:
                    csv_filename = 'hourly_pricing_trunc4.csv'
                else:
                    csv_filename = 'hourly_pricing.csv'
            with open(csv_filename) as f:

                reader = csv.DictReader(f)

                # open output CSV for writing
                with open('output_profit' + str(min_profit) + '_fee'
                        + str(100.0 * trade_fee) + '_vol' + str(daily_volume_pct)
                        + '.csv', mode='w') as fout:

                    # TODO: write all data to a single CSV
                    # initialize CSV header for hourly output data
                    if write:
                        writer = csv.writer(fout, delimiter=',',
                                quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        header = [ 'SECONDS' ]
                        for asset in all_assets:
                            header.append(asset)
                        header.append('HODL')
                        header.append('POOL %.2f%% fee' % (100.0 * trade_fee))
                        writer.writerow(header)

                    # read hourly price data from input CSV
                    for row in reader:

                        # update redemption prices
                        for asset in hodl_assets:
                            asset.extprice = float(row[asset.name])
                        init_pool.set_prices(row)
                        pool.set_prices(row)

                        # initialize pools according to weights
                        if first_pass:

                            for asset in hodl_assets:
                                asset.balance = init_liquidity / asset.extprice
                                if asset.name is 'ETH':
                                    amount_eth = asset.balance
                            init_pool.allocate(init_liquidity)
                            pool.allocate(init_liquidity)

                            first_pass = False

                        # or, rebalance the existing pool
                        else:

                            pool.rebalance()

                        # write hourly output data for the pool
                        times.append(float(row['SECONDS']))
                        hodl_liquidities.append(init_pool.get_liquidity() / 1000.0)
                        bal_earned = fee_factor * ratio_factor * bal_pct * pool.get_liquidity() / 1000.0
                        if len(bal_rewards[min_profit][trade_fee][daily_volume_pct]) > 0:
                            bal_total = bal_rewards[min_profit][trade_fee][daily_volume_pct][-1] + bal_earned
                        else:
                            bal_total = 0
                        bal_rewards[min_profit][trade_fee][daily_volume_pct].append(bal_total)
                        if 'ETH' in pool.assets:
                            eth_balances[min_profit][trade_fee][daily_volume_pct].append(pool.assets['ETH'].balance)
                        else:
                            eth_balances[min_profit][trade_fee][daily_volume_pct].append(0.0)
                        pool_liquidities[min_profit][trade_fee][daily_volume_pct].append(pool.get_liquidity() / 1000.0)
                        for asset in hodl_assets:
                            prices[asset.name].append(asset.get_liquidity() / 1000.0)
                        if pool.trades > trades:
                            trade_times[min_profit][trade_fee][daily_volume_pct].append(times[-1])
                        trades = pool.trades


                        if write:
                            row_out = [ float(row['SECONDS']) ]
                            for asset in hodl_assets:
                                row_out.append(asset.get_liquidity())
                            row_out.append(init_pool.get_liquidity())
                            row_out.append(pool.get_liquidity())
                            writer.writerow(row_out)

            # print user-friendly output to shell
            print('\tBP value (min_profit=$%.2f, trade_fee = %.2f%%, num_trades = %d, daily_volume_pct = %d%%) = %.2f'
                    % (min_profit, 100.0*trade_fee, pool.trades,
                    int(100.0*daily_volume_pct), pool.get_liquidity()))

# print HODL comparison data
for asset in hodl_assets:
    print('\t%s value = %.2f' % (asset.name, asset.get_liquidity()))
print('\tHODL value = %.2f' % init_pool.get_liquidity())

# plot
if plot:
    # convert epoch times to dates
    dateconv = np.vectorize(dt.datetime.fromtimestamp)
    dates = dateconv(np.array(times))

    plot_count = 1
    if plot_bal_rewards:
        plot_count += 1
    if plot_eth:
        plot_count += 1
    if plot_eth_balance:
        plot_count += 1
    if plot_impermanent_loss:
        plot_count += 1

    plot_position = 1

    # plot ETH price
    if plot_eth:
        plt.subplot(plot_count, 1, plot_position)
        plot_position += 1
        plt.plot_date(dates, np.array(prices['ETH']) * 1000.0 / amount_eth, '-', label='ETH', linewidth=plot_line_width)
        plt.title(plot_title)
        plt.ylabel('price (USD)')
        leg = plt.legend(prop={'size': plot_legend_font_size}, loc='upper left')
        for legobj in leg.legendHandles:
            legobj.set_linewidth(plot_legend_line_width)
        if plot_position <= plot_count:
            plt.tick_params(
                    axis='x',          # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom=False,      # ticks along the bottom edge are off
                    top=False,         # ticks along the top edge are off
                    labelbottom=False) # labels along the bottom edge are off

    # plot underlying assets
    plt.subplot(plot_count, 1, plot_position)
    plot_position += 1
    color = reset_plot_color()
    legend_labels = ()
    for asset in all_assets:
        if plot_eth and asset is 'ETH':
            continue
        plt.plot_date(dates, prices[asset], '-', color='C'+str(color), linewidth=plot_line_width)
        color = cycle_plot_color(color)
        legend_labels += (asset,)

    # plot HODL portfolio
    if plot_hodl:
        plt.plot_date(dates, hodl_liquidities, '-', color='C'+str(color), linewidth=2*plot_line_width)
        color = cycle_plot_color(color)
        legend_string = str(int(100.0 * pool_weights[0]))
        for i in range(1, len(pool_weights)):
            legend_string += ('/%d' % int(100.0 * pool_weights[i]))
        legend_string += ' HODL'
        legend_labels += (legend_string,)

    # plot various parametrized pools
    # for min_profit in min_profits:
    #     for trade_fee in trade_fees:
    #         for daily_volume in daily_volumes:
    #             plt.plot_date(dates, pool_liquidities[min_profit][trade_fee][daily_volume], '-', color='C'+str(color), linewidth=plot_line_width)
    #             color = cycle_plot_color(color)
    #             legend_string = 'POOL'
    #             if len(trade_fees) > 1:
    #                 legend_string += (' %.1f%% fee' % (100.0 * trade_fee))
    #             if len(daily_volumes) > 1:
    #                 legend_string += (' %.1f%% volume' % (100.0 * daily_volume))
    #             if len(min_profits) > 1:
    #                 legend_string += (' $%d profits' % int(min_profit))
    #             legend_labels += (legend_string,)
    plt.ylabel('value (kUSD)')
    leg = plt.legend(labels = legend_labels, prop={'size': plot_legend_font_size}, loc='upper left')
    for legobj in leg.legendHandles:
        legobj.set_linewidth(plot_legend_line_width)
    if not plot_eth:
        plt.title(plot_title)
    if plot_position <= plot_count:
        plt.tick_params(
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=False) # labels along the bottom edge are off

    # plot impermanent losses
    if plot_impermanent_loss:
        plt.subplot(plot_count, 1, plot_position)
        plot_position += 1
        color = reset_plot_color()
        legend_labels = ('HODL',)
        plt.plot_date(dates, np.zeros_like(hodl_liquidities), '-', color='C'+str(color), linewidth=plot_line_width)
        color = cycle_plot_color(color)
        for min_profit in min_profits:
            for trade_fee in trade_fees:
                for daily_volume in daily_volumes:
                    impermanent_loss = 100.0 * (np.array(pool_liquidities[min_profit][trade_fee][daily_volume]) - np.array(hodl_liquidities)) / np.array(hodl_liquidities)
                    plt.plot_date(dates, impermanent_loss, '-', color='C'+str(color), linewidth=plot_line_width)
                    color = cycle_plot_color(color)
                    legend_string = 'POOL'
                    if len(trade_fees) > 1:
                        legend_string += (' %.1f%% fee' % (100.0 * trade_fee))
                    if len(daily_volumes) > 1:
                        legend_string += (' %d%% volume' % int((100.0 * daily_volume)))
                    if len(min_profits) > 1:
                        legend_string += (' $%d profits' % int(min_profit))
                    legend_labels += (legend_string,)
        plt.ylabel('perf. vs HODL (%)')
        leg = plt.legend(labels = legend_labels, prop={'size': plot_legend_font_size}, loc='upper left')
        for legobj in leg.legendHandles:
            legobj.set_linewidth(plot_legend_line_width)
        if plot_position <= plot_count:
            plt.tick_params(
                    axis='x',          # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom=False,      # ticks along the bottom edge are off
                    top=False,         # ticks along the top edge are off
                    labelbottom=False) # labels along the bottom edge are off

    # plot changing ETH balance
    if plot_eth_balance:
        plt.subplot(plot_count, 1, plot_position)
        plot_position += 1
        color = reset_plot_color()
        legend_labels = ()
        color = cycle_plot_color(color)
        for min_profit in min_profits:
            for trade_fee in trade_fees:
                for daily_volume in daily_volumes:
                    plt.plot_date(dates, 100.0 * np.array(eth_balances[min_profit][trade_fee][daily_volume])/eth_balances[min_profit][trade_fee][daily_volume][0], '-', color='C'+str(color), linewidth=plot_line_width)
                    color = cycle_plot_color(color)
                    legend_string = 'POOL'
                    if len(trade_fees) > 1:
                        legend_string += (' %.1f%% fee' % (100.0 * trade_fee))
                    if len(daily_volumes) > 1:
                        legend_string += (' %d%% volume' % int((100.0 * daily_volume)))
                    if len(min_profits) > 1:
                        legend_string += (' $%d profits' % int(min_profit))
                    legend_labels += (legend_string,)
        plt.ylabel('ETH bal (%)')
        leg = plt.legend(labels = legend_labels, prop={'size': plot_legend_font_size}, loc='upper left')
        for legobj in leg.legendHandles:
            legobj.set_linewidth(plot_legend_line_width)
        if plot_position <= plot_count:
            plt.tick_params(
                    axis='x',          # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom=False,      # ticks along the bottom edge are off
                    top=False,         # ticks along the top edge are off
                    labelbottom=False) # labels along the bottom edge are off

    # plot BAL rewards
    if plot_bal_rewards:
        plt.subplot(plot_count, 1, plot_position)
        plot_position += 1
        color = reset_plot_color()
        legend_labels = ()
        color = cycle_plot_color(color)
        for min_profit in min_profits:
            for trade_fee in trade_fees:
                for daily_volume in daily_volumes:
                    plt.plot_date(dates, 100.0 * np.array(bal_rewards[min_profit][trade_fee][daily_volume])/pool_liquidities[min_profit][trade_fee][daily_volume][0], '-', color='C'+str(color), linewidth=plot_line_width)
                    color = cycle_plot_color(color)
                    legend_string = 'POOL'
                    if len(trade_fees) > 1:
                        legend_string += (' %.1f%% fee' % (100.0 * trade_fee))
                    if len(daily_volumes) > 1:
                        legend_string += (' %d%% volume' % int((100.0 * daily_volume)))
                    if len(min_profits) > 1:
                        legend_string += (' $%d profits' % int(min_profit))
                    legend_labels += (legend_string,)
        plt.ylabel('BAL rewards (%)')
        leg = plt.legend(labels = legend_labels, prop={'size': plot_legend_font_size}, loc='upper left')
        for legobj in leg.legendHandles:
            legobj.set_linewidth(plot_legend_line_width)
        if plot_position <= plot_count:
            plt.tick_params(
                    axis='x',          # changes apply to the x-axis
                    which='both',      # both major and minor ticks are affected
                    bottom=False,      # ticks along the bottom edge are off
                    top=False,         # ticks along the top edge are off
                    labelbottom=False) # labels along the bottom edge are off

    # format
    # plt.xlabel('date')
    plt.subplots_adjust(left=plot_margin_left, bottom=plot_margin_bottom, right=1.0-plot_margin_right, top=1.0-plot_margin_top, wspace=plot_margin_horz, hspace=plot_margin_vert)
    plt.savefig(plot_filename, dpi=384)
    plt.show()
