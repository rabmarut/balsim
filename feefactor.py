#!/usr/bin/env python

import math
import matplotlib.pyplot as plt
import numpy as np


''' params '''
plot_legend_font_size = 6
plot_legend_line_width = 1
plot_line_width = 1.0
plot_margin_bottom = 0.1
plot_margin_horz = 0.1
plot_margin_left = 0.1
plot_margin_right = 0.02
plot_margin_top = 0.06
plot_margin_vert = 0.0


''' functions '''
def computeFeeFactor(coeff, power, fee):
    return math.exp(-((coeff * fee) ** power))


''' main '''

# set up x-axis (fees)
fee_max = 10.0
fee_min = 0.0
fee_step = 0.01
fees = fee_step * np.array(range(int(fee_min / fee_step), int(fee_max / fee_step)))

# set up y-axis for polynomial version
coeffs = [ -0.009, 0.0, 1.0 ]
n = len(coeffs)
order = n - 1

# compute factors with varying coefficients
polys = []
for fee in fees:
    poly = 0
    for i in range(0, n):
        poly += coeffs[i] * (fee ** (order - i))
    polys.append(poly)

# plot polynomial version
plt.plot(fees, polys, linewidth=plot_line_width)
plt.grid(color='k', ls = '-.', lw = 0.1)
plt.title('Fee Factor')
plt.xlabel('fee (%)')
plt.ylabel('feeFactor')
plt.xlim(fee_min, fee_max)
plt.ylim(0.0, 1.0)
plt.savefig('fee_factor_poly.png', dpi=384)
plt.show()

# compute factors with varying coefficients and powers
# coeffs = [ 0.2, 0.25, 0.35, 0.5 ]
# powers = [ 1.0, 2.0, 3.0 ]
coeffs = [ 0.25 ]
powers = [ 2.0 ]
exps = {}
for coeff in coeffs:
    exps[coeff] = {}
    for power in powers:
        exps[coeff][power] = []
        for fee in fees:
            exps[coeff][power].append(computeFeeFactor(coeff, power, fee))

# plot all
plt.figure()
n = len(powers)
for i in range(0, n):
    # plot power in a unique subplot
    power = powers[i]
    plt.subplot(n, 1, i + 1)
    if i == 0:
        plt.title('Fee Factor')
    legend_labels = ()

    # plot a curve for each coefficient
    for coeff in coeffs:
        plt.plot(fees, exps[coeff][power], linewidth=plot_line_width)
        legend_labels += ('k=%.2f' % coeff,)
        plt.grid(color='k', ls = '-.', lw = 0.1)

    # format
    # plt.ylabel('pow=%.1f' % power)
    plt.ylabel('feeFactor')
    plt.xlim(fee_min, fee_max)
    plt.ylim(0.0, 1.0)
    # leg = plt.legend(labels=legend_labels, prop={'size': plot_legend_font_size}, loc='upper right')
    # for legobj in leg.legendHandles:
    #     legobj.set_linewidth(plot_legend_line_width)

    # remove all y ticks
    # plt.tick_params(axis='y', which='both', left=False, right=False, labelleft=False)
    if i < n - 1:
        # remove all x ticks but from the bottom subplot
        plt.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    else:
        # label bottom subplot
        plt.xlabel('fee (%)')

# format and display
plt.subplots_adjust(left=plot_margin_left, bottom=plot_margin_bottom, right=1.0-plot_margin_right, top=1.0-plot_margin_top, wspace=plot_margin_horz, hspace=plot_margin_vert)
plt.savefig('fee_factor_exp.png', dpi=384)
plt.show()

# print common fees to console
coeffs = [ 0.25, 0.5 ]
power = 2
fees = [ 0.1, 0.2, 0.3, 0.5, 1.0, 2.0, 3.0, 5.0, 10.0 ]
for fee in fees:
    oldFactor = computeFeeFactor(coeffs[1], power, fee)
    newFactor = computeFeeFactor(coeffs[0], power, fee)
    string = ' ' if fee < 10.0 else ''
    print('%s%.1f%% : %.2f => %.2f' % (string, fee, oldFactor, newFactor))
