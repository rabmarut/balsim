#!/usr/bin/env python

import math
import matplotlib.pyplot as plt
import numpy as np


''' params '''
plot_legend_font_size = 6
plot_legend_line_width = 1
plot_line_width = 1.0
plot_margin_bottom = 0.1
plot_margin_horz = 0.1
plot_margin_left = 0.04
plot_margin_right = 0.02
plot_margin_top = 0.06
plot_margin_vert = 0.0


''' functions '''
def computeRatioFactor(weights):
    ratioFactorSum = 0
    pairWeightSum = 0
    n = len(weights)
    for i in range(0, n):
        for j in range(i+1, n):
            pairWeight = weights[i] * weights[j]
            normalizedWeight1 = weights[i] / (weights[i] + weights[j])
            normalizedWeight2 = weights[j] / (weights[i] + weights[j])
            ratioFactorSum += (4 * normalizedWeight1 * normalizedWeight2) * pairWeight
            pairWeightSum += pairWeight
    return ratioFactorSum / pairWeightSum


''' main '''

# set up x-axis (fees)
weight_max = 0.98
weight_min = 0.50
weight_step = 0.001

# compute factors with varying ratios
factors = []
weights = []
weight = weight_min
while weight <= weight_max:
    factors.append(computeRatioFactor([weight, 1.0 - weight]))
    weights.append(weight)
    weight += weight_step

# plot
plt.plot(100.0 * np.array(weights), factors, linewidth=plot_line_width)

# format
plt.grid(color='k', ls = '-.', lw = 0.1)
plt.title('Ratio Factor')
plt.xlabel('weight1 (%)')
plt.ylabel('ratioFactor')
plt.xlim(100.0 * weight_min, 100.0 * weight_max)
plt.ylim(0.0, 1.0)
plt.savefig('ratio_factor.png', dpi=384)
plt.show()

# print common portfolios to console
assetWeights = [ [ 0.5, 0.5 ] ]
assetWeights.append([ 0.6, 0.4 ])
assetWeights.append([ 0.7, 0.3 ])
assetWeights.append([ 0.75, 0.25 ])
assetWeights.append([ 0.8, 0.2 ])
assetWeights.append([ 0.9, 0.1 ])
assetWeights.append([ 0.98, 0.02 ])
assetWeights.append([ 1.0/3.0, 1.0/3.0, 1.0/3.0 ])
assetWeights.append([ 0.25, 0.25, 0.25, 0.25 ])
assetWeights.append([ 0.2, 0.2, 0.2, 0.2, 0.2 ])
assetWeights.append([ 0.5, 0.2, 0.1, 0.05, 0.05, 0.04, 0.03, 0.03 ])
assetWeights.append([ 0.55, 0.15, 0.15, 0.15 ])
assetWeights.append([ 0.5, 0.2, 0.1, 0.1, 0.1 ])
assetWeights.append([ 0.4, 0.3, 0.1, 0.1, 0.1 ])
assetWeights.append([ 0.35, 0.35, 0.1, 0.1, 0.1 ])
assetWeights.append([ 0.49, 0.49, 0.02 ])
assetWeights.append([ 0.48, 0.48, 0.04 ])
assetWeights.append([ 0.47, 0.47, 0.06 ])
assetWeights.append([ 0.46, 0.46, 0.08 ])
assetWeights.append([ 0.45, 0.45, 0.1 ])
assetWeights.append([ 0.44, 0.44, 0.12 ])
assetWeights.append([ 0.43, 0.43, 0.14 ])
assetWeights.append([ 0.42, 0.42, 0.16 ])
assetWeights.append([ 0.41, 0.41, 0.18 ])
assetWeights.append([ 0.4, 0.4, 0.2 ])
assetWeights.append([ 0.39, 0.39, 0.22 ])
assetWeights.append([ 0.38, 0.38, 0.24 ])
assetWeights.append([ 0.37, 0.37, 0.26 ])
assetWeights.append([ 0.36, 0.36, 0.28 ])
assetWeights.append([ 0.35, 0.35, 0.30 ])
assetWeights.append([ 0.34, 0.34, 0.32 ])
assetWeights.append([ 0.4, 0.3, 0.3 ])
assetWeights.append([ 0.5, 0.25, 0.25 ])
assetWeights.append([ 0.575, 0.2, 0.075, 0.075, 0.075 ])
for weights in assetWeights:
    factor = computeRatioFactor(weights)
    print(weights, ' => ', factor)
