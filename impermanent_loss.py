#!/usr/bin/env python

from bal_model import Asset, Pool

# set up pool: (name, weight, price_change)
assets = [
        Asset('DAI', 0.98, 1.00),
        Asset('X', 0.02, 0.01),
]
print()
print('pool composition: %s' % assets)
print()

pool = Pool(0.0, 0.0, 0.0)
for asset in assets:
    asset.balance = asset.weight
    pool.add_asset(asset)
pool.update_weights()

# predict price changes
pool_delta = 1.0
for key, asset in sorted(pool.assets.items(), key=lambda item: item[0]):
    pool_delta *= asset.extprice ** asset.weight
pool_pct_gain = 100.0 * (pool_delta - 1.0)
sign = '+' if pool_pct_gain > 0 else ''
print('Pool: gain/loss = %s%.2f%%' % (sign, pool_pct_gain))

# compute equivalent HODL changes
hodl_delta = 0.0
for key, asset in sorted(pool.assets.items(), key=lambda item: item[0]):
    hodl_delta += asset.extprice * asset.weight
hodl_pct_gain = 100.0 * (hodl_delta - 1.0)
sign = '+' if hodl_pct_gain > 0 else ''
print('HODL: gain/loss = %s%.2f%%' % (sign, hodl_pct_gain))

# compute impermanent loss
impermanent_loss = 100.0 * ((pool_delta / hodl_delta) - 1)
print('impermanent loss: %.2f%%' % impermanent_loss)
print()

# simulate final holdings
thresh = 0.0001
for i in range(0, 1000):
    pool.rebalance()
    done = True
    pool_assets = list(pool.assets.values())
    n = len(pool_assets)
    for j in range(0, n):
        for k in range(j+1, n):
            spot = pool.compute_spot_price(pool_assets[j], pool_assets[k])
            market = pool.compute_redemption_price(pool_assets[j], pool_assets[k])
            if abs(spot - market) / spot > thresh:
                done = False
    if done is True:
        break

for key, asset in sorted(pool.assets.items(), key=lambda item: item[0]):
    balance_change = 100.0 * ((asset.balance / asset.weight) - 1.0)
    sign = '+' if balance_change > 0 else ''
    print('%s balance: %.2f -> %.2f (%s%d%%)' % (asset.name, asset.weight, asset.balance, sign, balance_change))
print()
