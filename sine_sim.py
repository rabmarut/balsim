#!/usr/bin/env python


from bal_model import Asset, Pool
import csv
import math
import matplotlib.pyplot as plt


# parameters
plot = True
price_amp_pct = 0.5
price_med = 200.0
show_discrete_trades = False
t_begin = 0.0
t_end = 50.0
t_step = 0.01
write = True

init_liquidity = price_med


# initialize loop variables
pool_liquidities = {}
trade_times = {}


# loop over fees
trade_fees = [ 0.0, 0.003, 0.01, 0.05, 0.1 ]
for trade_fee in trade_fees:

    pool_liquidities[trade_fee] = [ init_liquidity ]
    trade_times[trade_fee] = [ 0.0 ]

    # initialize unchanging reference pool
    init_pool = Pool(0.0, 0.0, 0.0)
    init_pool.add_asset('ETH')
    init_pool.add_asset('USD')
    init_pool.update_weights()

    # initialize rebalancing pool
    pool = Pool(0.0, 0.0, trade_fee)
    pool.add_asset('ETH')
    pool.add_asset('USD')
    pool.update_weights()

    # initialize prices
    init_pool.assets['ETH'].extprice = price_med
    init_pool.assets['USD'].extprice = 1.0
    pool.assets['ETH'].extprice = price_med
    pool.assets['USD'].extprice = 1.0

    # initialize allocation
    init_pool.allocate(init_liquidity)
    pool.allocate(init_liquidity)

    # initialize time loop
    hodl_liquidities = [ ]
    prices = [ ]
    times = [ ]
    trades = 0

    # loop over time
    for i in range(int(t_begin / t_step), int(t_end / t_step)):

        # compute time
        times.append(i * t_step)

        # compute new token price
        prices.append(price_med * (1.0 + price_amp_pct * math.sin(times[-1])))
        init_pool.assets['ETH'].extprice = prices[-1]
        pool.assets['ETH'].extprice = prices[-1]
        pool.rebalance()

        # extract pool liquidity
        hodl_liquidities.append(init_pool.get_liquidity())
        if show_discrete_trades:
            if pool.trades > trades:
                pool_liquidities[trade_fee].append(pool_liquidities[trade_fee][-1])
                trade_times[trade_fee].append(times[-1])
                pool_liquidities[trade_fee].append(pool.get_liquidity())
                trade_times[trade_fee].append(times[-1])
        else:
            pool_liquidities[trade_fee].append(pool.get_liquidity())
            trade_times[trade_fee].append(times[-1])

        # keep track of total trades
        trades = pool.trades

# plot
if plot:
    plt.plot(times, prices)
    plt.plot(times, hodl_liquidities)
    legend_labels = ('ETH', '50/50 HODL')
    for trade_fee in trade_fees:
        plt.plot(trade_times[trade_fee], pool_liquidities[trade_fee])
        legend_labels += (('POOL %.1f%%' % (100.0 * trade_fee)),)
    plt.legend(labels = legend_labels)
    plt.title('Sinusoidal ETH price')
    plt.xlabel('time (hours)')
    plt.ylabel('value (USD)')
    plt.savefig('eth_sine.png', dpi=384)
    plt.show()

# write to file
if write:
    with open('output_sine.csv', mode='w') as fout:
        writer = csv.writer(fout, delimiter=',', quotechar='"',
                quoting=csv.QUOTE_MINIMAL)

        # write header
        header = [ 'ETH', '50/50 HODL' ]
        for trade_fee in trade_fees:
            header.append('POOL %.1f%%' % (100.0 * trade_fee))
        writer.writerow(header)

        # write rows
        for i in range(0, len(times)):
            row_out = [ prices[i], hodl_liquidities[i] ]
            for trade_fee in trade_fees:
                row_out.append(pool_liquidities[trade_fee][i])
            writer.writerow(row_out)
