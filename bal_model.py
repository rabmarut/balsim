debug = False


class Asset:

    def __eq__(self, other):
        return self.name == other.name

    def __init__(self, name, denorm_weight=1.0, extprice=0.0, balance=0.0):
        self.name = name
        self.balance = balance
        self.extprice = extprice
        self.weight = denorm_weight

    def __repr__(self):
        return str(self)

    def __str__(self):
        return '(%s: %.1f%% -> %.2fx)' % (self.name, 100.0 * self.weight, self.extprice)

    def get_liquidity(self):
        return self.balance * self.extprice


class Pool:

    def __init__(self, daily_volume_pct, min_profit, trade_fee):
        self.assets = {}
        self.hourly_volume_pct = (1.0 + daily_volume_pct) ** (1.0 / 24.0) - 1.0
        self.min_profit = min_profit
        self.trade_fee = trade_fee
        self.trades = 0

    def add_asset(self, asset, denorm_weight=1):
        if isinstance(asset, str):
            self.assets[asset] = Asset(asset, denorm_weight)
        else:
            self.assets[asset.name] = asset

    def allocate(self, total_liquidity):
        for key, asset in self.assets.items():
            asset.balance = asset.weight * total_liquidity / asset.extprice

    def compute_in_given_out(self, asset_in, asset_out, amount_out):
        return asset_in.balance \
                * ((asset_out.balance / (asset_out.balance - amount_out)) \
                 ** (asset_out.weight / asset_in.weight) - 1.0) \
                * (1.0 / (1.0 - self.trade_fee))

    def compute_in_given_price(self, asset_in, asset_out):
        price = self.compute_redemption_price(asset_in, asset_out)
        return self.compute_in_given_price(asset_in, asset_out, price)

    def compute_in_given_price(self, asset_in, asset_out, price):
        market_price = self.compute_spot_price(asset_in, asset_out)
        return asset_in.balance * ((price / market_price) \
                ** (asset_out.weight / (asset_out.weight + asset_in.weight)) - 1.0)

    def compute_out_given_in(self, asset_in, asset_out, amount_in):
        return asset_out.balance * (1.0 - (asset_in.balance \
                / (asset_in.balance + amount_in * (1.0 - self.trade_fee))) \
                ** (asset_in.weight / asset_out.weight))

    def compute_redemption_price(self, asset_in, asset_out):
        return (asset_out.extprice / asset_in.extprice)

    def compute_spot_price(self, asset_in, asset_out):
        return (asset_in.balance / asset_in.weight) \
                / (asset_out.balance / asset_out.weight)

    def get_liquidity(self):
        liquidity = 0
        for key, asset in self.assets.items():
            liquidity += asset.get_liquidity()
        return liquidity

    def rebalance(self):
        assets = list(self.assets.values())
        n = len(assets)
        hourly_trades = self.hourly_volume_pct * self.get_liquidity() * self.trade_fee / n

        # add fixed volume per hour to simulate liquid trading beyond arbitrage
        for i in range(0, n):
            assets[i].balance += hourly_trades / assets[i].extprice

        # simulate arbitrage among assets
        for i in range(0, n):
            for j in range(i+1, n):
                self.trade(assets[i], assets[j])

    def set_prices(self, prices):
        for key, asset in self.assets.items():
            asset.extprice = float(prices[key])

    def trade(self, asset1, asset2):
        # compute relative pricing on both markets (internal and external)
        # price is asset2 in terms of asset1
        market_price = self.compute_spot_price(asset1, asset2)
        redemption_price = self.compute_redemption_price(asset1, asset2)

        # buy and sell price are used by arbitrageurs to detect maximal profit
        buy_price = market_price / (1.0 - self.trade_fee)
        sell_price = market_price * (1.0 - self.trade_fee)

        # the more valuable asset (to redeem externally) is asset_out
        if redemption_price > buy_price:
            asset_in = asset1
            asset_out = asset2
            target_price = redemption_price * (1.0 - self.trade_fee)

            # compute trade size required for equalizing arbitrage
            amount_in = self.compute_in_given_price(asset_in, asset_out, target_price)
        elif redemption_price < sell_price:
            asset_in = asset2
            asset_out = asset1
            target_price = redemption_price / (1.0 - self.trade_fee)

            # compute trade size required for equalizing arbitrage
            amount_in = self.compute_in_given_price(asset_in, asset_out, 1.0 / target_price)
        else:
            return

        # compute return for arbitrageur
        amount_out = self.compute_out_given_in(asset_in, asset_out, amount_in)

        # compute in/out in terms of USD for the arbitrageur
        value_in = amount_in * asset_in.extprice
        value_out = amount_out * asset_out.extprice

        if debug:
            print('trade fee = %.2f' % self.trade_fee)
            print('asset1 balance = %.2f' % asset1.balance)
            print('asset2 balance = %.2f' % asset2.balance)
            print('asset1 redemption price (USD) = %.4f' % asset1.extprice)
            print('asset2 redemption price (USD) = %.4f' % asset2.extprice)
            print('relative market price = %.4f' % market_price)
            print('relative redemption price = %.4f' % redemption_price)
            print('buy price = %.4f' % buy_price)
            print('sell price = %.4f' % sell_price)
            print('target price = %.4f' % target_price)
            print('amount in = %.2f' % amount_in)
            print('amount out = %.2f' % amount_out)
            print('value in (USD) = %.2f' % value_in)
            print('value out (USD) = %.2f' % value_out)

        # only simulate arbitrage above a fixed USD profit
        if value_out - value_in > self.min_profit:
            self.trades += 1
            asset_in.balance += amount_in
            asset_out.balance -= amount_out
            if debug:
                print('trading...')
                print('asset in balance = %.2f' % asset_in.balance)
                print('asset out balance = %.2f' % asset_out.balance)

        if debug:
            input()

    def update_weights(self):
        sum_weights = 0
        for key, asset in self.assets.items():
            sum_weights += asset.weight
        for key, asset in self.assets.items():
            asset.weight /= sum_weights
